// Default URL for triggering event grid function in the local environment.
// http://localhost:7071/runtime/webhooks/EventGrid?functionName={functionname}
using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.EventGrid.Models;
using Microsoft.Azure.WebJobs.Extensions.EventGrid;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace FunctionApp2
{
    public static class Function1
    {
        [FunctionName("EventGridEventTriggerFunction")]
        public static void Run([EventGridTrigger]EventGridEvent eventGridEvent, ILogger log)
        {
            log.LogInformation("Event received {type} from {subject}", eventGridEvent.EventType, eventGridEvent.Subject);
        }

        [FunctionName("EventGridEventTriggerFunction1")]
        public static void RunEvent1([EventGridTrigger] EventGridEvent eventGridEvent, ILogger log)
        {
            string jsonData = JsonConvert.SerializeObject(eventGridEvent);
            log.LogInformation("Event received {type} from {subject} and Data=>{data}", eventGridEvent.EventType, eventGridEvent.Subject, jsonData);
        }

        [FunctionName("EventGridEventTriggerFunction2")]
        public static void RunEvent2([EventGridTrigger] EventGridEvent eventGridEvent, ILogger log)
        {
            string jsonData = JsonConvert.SerializeObject(eventGridEvent);
            log.LogInformation("Event received {type} from {subject} and Data=>{data}", eventGridEvent.EventType, eventGridEvent.Subject, "Jitendra Kumar Dubey");
        }

        [FunctionName("Function1")]
        public static void RunFunction1([EventGridTrigger] EventGridEvent eventGridEvent, ILogger log)
        {
            log.LogInformation("Event fired by subscriber=> {data}", eventGridEvent.Data.ToString());
        }
    }
}
